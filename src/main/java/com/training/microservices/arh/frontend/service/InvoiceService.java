package com.training.microservices.arh.frontend.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.microservices.arh.frontend.dto.Customer;

@FeignClient(name = "invoice", fallback = InvoiceServiceFallback.class)
public interface InvoiceService {

    @GetMapping("/customer/")
    public Iterable<Customer> ambilDataCustomer();
}