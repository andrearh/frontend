package com.training.microservices.arh.frontend.dto;

import lombok.Data;

@Data
public class Customer {
	private String id;
	private String name;
	private String number;
	private String email;
	private String mobilePhone;
}
