package com.training.microservices.arh.frontend.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.training.microservices.arh.frontend.dto.Customer;

@Service
public class InvoiceServiceFallback implements InvoiceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceServiceFallback.class);

    @Override
    public Iterable<Customer> ambilDataCustomer() {
        LOGGER.info("Menjalankan fallback ambil data customer");
        return new ArrayList<>();
    }
}
