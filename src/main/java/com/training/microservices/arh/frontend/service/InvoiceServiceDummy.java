package com.training.microservices.arh.frontend.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.training.microservices.arh.frontend.dto.Customer;

@Profile("default")
@Service
public class InvoiceServiceDummy {
	public Iterable<Customer> ambilDataCustomer(){
		List<Customer> dataDummy = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
            Customer c = new Customer();
            c.setId("id"+i);
            c.setNumber("D-00"+i);
            c.setName("Dummy Data 00"+i);
            c.setEmail(i+"@coba.com");
            c.setMobilePhone("0812223344"+i);
            dataDummy.add(c);
		}
		return dataDummy;
	}
}
