package com.training.microservices.arh.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.training.microservices.arh.frontend.dto.NotificationRequest;
import com.training.microservices.arh.frontend.service.InvoiceService;
import com.training.microservices.arh.frontend.service.KafkaProducerService;

@Controller
public class CustomerController {

    @Autowired private InvoiceService invoiceService;
    @Autowired private KafkaProducerService kafkaProducerService;

    @GetMapping("/customer/list")
    public ModelMap dataCustomer() {
        return new ModelMap().addAttribute("dataCustomer", invoiceService.ambilDataCustomer());
    }
    
    @GetMapping("/customer/notifikasi")
    public void tampilkanFormNotifikasi() {

    }

    @PostMapping("/customer/notifikasi")
    public String prosesFormNotifikasi(@ModelAttribute NotificationRequest req) {
        kafkaProducerService.kirimNotifikasi(req);
        return "redirect:notifikasi";
    }

}